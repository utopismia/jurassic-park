<?php require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug());
    $twig->addFilter(new Twig_Filter('markdown', function ($string) {
        return renderHTMLFromMarkdown($string);
    }));
});

Flight::map('render', function ($template, $data = array()) {
    Flight::view()->display($template, $data);
});

Flight::route('/dinausours/', function () {
    $data = [
        'dinausours' => getdinausours()(),
    ];
    Flight::render('dinausour.twig', $data);
});

Flight::route('/page/', function () {
    $data = [
        'page_content' => getPageContent("test"),
    ];
    Flight::render('page.twig', $data);
});

Flight::start();