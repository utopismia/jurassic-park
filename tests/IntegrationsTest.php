    <?php require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest
{

    public function test_index()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("Jurassic Park", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_dinausours()
    {
        $response = $this->make_request("GET", "/dinausours/");
        $dinausours = getdinausours();
        $this->assertEquals(200, $response->getStatusCode());
        $body = $response->getBody()->getContents();
        foreach ($dinausours as $dinausour) {
            $this->assertStringContainsString($dinausour->name, $body);
            $this->assertStringContainsString($dinausour->avatar, $body);
        }
    }

    public function test_pages()
    {
        $response = $this->make_request("GET", "/page/");
        $this->assertEquals(200, $response->getStatusCode());
        $body = $response->getBody()->getContents();
        $html_from_test_page_file = renderHTMLFromMarkdown(readFileContent("pages/test.md"));
        $this->assertStringContainsString($html_from_test_page_file, $body);
    }
}